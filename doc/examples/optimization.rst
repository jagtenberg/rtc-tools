Optimization examples
=====================

This section provides examples demonstrating key features of RTC-Tools optimization.

.. toctree::
   :maxdepth: 2

   optimization/basic
   optimization/mixed_integer
   optimization/goal_programming
   optimization/lookup_table
   optimization/ensemble
   optimization/cascading_channels
   optimization/channel_pulse
   optimization/channel_wave_damping
