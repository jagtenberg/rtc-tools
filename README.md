This is RTC-Tools 2, a toolbox for control and optimization of water systems.

Visit our website at:
	https://oss.deltares.nl/web/rtc-tools/home

[![Pipeline](https://gitlab.com/deltares/rtc-tools/badges/master/pipeline.svg)](https://gitlab.com/deltares/rtc-tools/commits/master)
[![Coverage](https://codecov.io/gl/deltares/rtc-tools/branch/master/graph/badge.svg)](https://codecov.io/gl/deltares/rtc-tools)

## Install

```bash
pip install rtc-tools
```

Typically one also wants to use RTC-Tools with the RTC-Tools Channel Flow library, in which case the installation command is

```bash
pip install rtc-tools rtc-tools-channel-flow
```

## Documentation

Documentation and examples can be found on [readthedocs](https://rtc-tools.readthedocs.io).
